
name := "udemy-akka-essentials"

version := "0.1"

scalaVersion := "2.12.8"

val akkaVersion = "2.5.13"

libraryDependencies ++= Seq(
  "com.typesafe.akka"%%"akka-actor"%akkaVersion,
  "com.typesafe.akka"%%"akka-testkit"%akkaVersion,
  "org.scalatest"%%"scalatest"%"3.0.5",
  "com.typesafe.akka" %% "akka-stream" %akkaVersion,
  "com.typesafe.akka"%%"akka-remote"%akkaVersion,
  "com.typesafe.akka" %% "akka-persistence" %akkaVersion,
  "org.iq80.leveldb"            % "leveldb"          % "0.7",
  "org.fusesource.leveldbjni"   % "leveldbjni-all"   % "1.8"
)

