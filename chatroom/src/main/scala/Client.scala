import java.util.Scanner

import akka.actor.{Actor, ActorSystem, PoisonPill, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}
import com.typesafe.config.ConfigFactory

object ClientApp extends App {
  val scanner = new Scanner(System.in)
  print("Enter port: ")
  val port = scanner.nextLine

  print("enter username: ")
  val name = scanner.nextLine

  val configString = """
           akka.persistence.journal.plugin = "akka.persistence.journal.leveldb"
           akka.persistence.snapshot-store.plugin = "akka.persistence.snapshot-store.local"
           akka.persistence.journal.leveldb.dir = "target/example/journal/""" + name + """"
           akka.persistence.snapshot-store.local.dir = "target/example/snapshots"
           akka.persistence.journal.leveldb.native = false
           akka {
             actor {
               provider = "akka.remote.RemoteActorRefProvider"
               warn-about-java-serializer-usage = false
             }
             remote {
               transport = "akka.remote.netty.NettyRemoteTransport"
               netty.tcp {
                 hostname = "127.0.0.1"
                 port = """ +  port +
              """
                 port = ${?AKKA_TCP_PORT}
               }
             }
           }
        """
  val customConf = ConfigFactory.parseString(configString)
  val clientSystem = ActorSystem("Remote", ConfigFactory.load(customConf))
  val serverReference = clientSystem.actorSelection("akka.tcp://ServerApplication@" + "127.0.0.1" + ":" + 5150 + "/user/server")

  val client = clientSystem.actorOf(Props(new Client(name)))
  client ! LogIn(serverReference)
  client ! "print"

  while (true) {

    print("Enter message: ")
    val input = scanner.nextLine
    val tag = input.split(" ")
    if (tag.length > 0 && tag(0).split("@").length > 1) {
      val tagName = tag(0).split("@")(1)
      client ! SendByTag(serverReference, input, tagName)
    }
    else {
      client ! Send(serverReference, input)
    }

  }
}

case class Evt(data: String)

case class State(events: List[String] = Nil) {
  def updated(evt: Evt): State = copy(evt.data :: events)
  def size: Int = events.length
  override def toString: String = {
    var messages = ""
    if (events.length > 0) {
      events.reverse.foreach(e => messages += e + "\n")
    }
    messages
  }
}

class Client(val username: String) extends PersistentActor {

  override def persistenceId = username

  var state = State()

  def updateState(event: Evt): Unit =
    state = state.updated(event)

  def numEvents =
    state.size

  val receiveRecover: Receive = {
    case evt: Evt                                 => updateState(evt)
    case SnapshotOffer(_, snapshot: State) => state = snapshot
  }


  val receiveCommand: Receive = {
    case LogIn(server) =>
      server ! Connect(username)
    case NewMsg(from, msg) => {
      if (from != username) {
        val data = f"*+* $from%s: $msg%s"
        println(data)
        persist(Evt(s"${data}")) { event =>
          updateState(event)
          context.system.eventStream.publish(event)
        }
      }
      else {
        val data = f"*-* $from%s: $msg%s"
        println(data)
        persist(Evt(s"${data}")) { event =>
          updateState(event)
          context.system.eventStream.publish(event)
        }
      }
    }
    case Send(server, msg) => server ! Broadcast(msg)
    case SendByTag(server, msg, tagName) =>
      server ! BroadcastByTag(msg, tagName)
    case Info(msg) => {
      val data = f"$msg%s"
      println(data)
      persist(Evt(s"${data}")) { event =>
        updateState(event)
        context.system.eventStream.publish(event)
      }
    }
    case "snap"  => saveSnapshot(state)
    case "print" => println(state)
    case Disconnect => {
      self ! PoisonPill
    }
  }
}


/*
class Client(val username: String) extends Actor {

  override def receive: Receive = {
    case LogIn(server) =>
      server ! Connect(username)
    case NewMsg(from, msg) => {
      if (from != username) {
        val data = f"*+* $from%s: $msg%s"
        println(data)
      }
      else {
        val data = f"*-* $from%s: $msg%s"
        println(data)
      }
    }
    case Send(server, msg) => server ! Broadcast(msg)
    case SendByTag(server, msg, tagName) =>
      server ! BroadcastByTag(msg, tagName)
    case Info(msg) => {
      val data = f"$msg%s"
      println(data)
    }
    case Disconnect => {
      self ! PoisonPill
    }
  }
}
*/