import java.util.Scanner

import akka.actor.{Actor, ActorRef, ActorSelection, ActorSystem, Props, Terminated}
import com.typesafe.config.ConfigFactory

object ServerApp extends App {
  var originalSender: ActorRef = _

  val configString = """akka {
                         actor {
                           provider = "akka.remote.RemoteActorRefProvider"
                           warn-about-java-serializer-usage = false
                          }
                          remote {
                            transport = "akka.remote.netty.NettyRemoteTransport"
                            netty.tcp {
                              hostname = "127.0.0.1"
                             port = 5150
                              port = ${?AKKA_TCP_PORT}
                            }
                          }
                       }"""
  val customConf = ConfigFactory.parseString(configString)
  val serverSystem = ActorSystem("ServerApplication", ConfigFactory.load(customConf))
  val serverActor = serverSystem.actorOf(Props[Server], "server")
  val scanner = new Scanner(System.in)
  println("Waiting for message from Local")
}

class Server extends Actor {

  var clients = List[(String, ActorRef)]();

  override def receive: Receive = {
    case Connect(username) => {
      broadcast(Info(f"$username%s joined the chat"))
      clients = (username,sender) :: clients
      context.watch(sender)
      println(context.self.path)
    }
    case Broadcast(msg) => {
      val username = getUsername(sender)
      broadcast(NewMsg(username, msg))
    }
    case BroadcastByTag(msg, tagName) => {
      val usernameSender = getUsername(sender)
      val actorTagged = getActor(tagName)
      println(s"actorTagged: $actorTagged")
      actorTagged ! NewMsg(usernameSender, msg)
      sender() ! NewMsg(usernameSender, msg)
    }
    case Terminated(client) => {
      val username = getUsername(client)
      clients = clients.filter(sender != _._2)
      broadcast(Info(f"$username%s left the chat"))
    }
  }

  def broadcast(msg: Msg) {
    clients.foreach(x => x._2 ! msg)
  }

  def getUsername(actor: ActorRef): String = {
    clients.filter(actor == _._2).head._1
  }

  def getActor(userName: String): ActorRef = {
    clients.filter(userName == _._1).head._2
  }
}

abstract class Msg

case class LogIn(server: ActorSelection)  extends Msg
case class Send(server: ActorSelection, msg: String) extends Msg
case class SendByTag(server: ActorSelection, msg: String, tagName: String) extends Msg
case class NewMsg(from: String, msg: String) extends Msg
case class Info(msg: String) extends Msg

case class Connect(username: String) extends Msg
case class Broadcast(msg: String) extends Msg
case class BroadcastByTag(msg: String, tagName: String) extends Msg
case object Disconnect extends Msg